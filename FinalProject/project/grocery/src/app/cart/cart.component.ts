import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  total: number;
  emailId: any;
  cartItems: any[];

  constructor(private service: CustomerService, private router: Router, private customerService: CustomerService, private cdr: ChangeDetectorRef) {
    this.cartItems = [];
    this.total = 0;

    this.emailId = localStorage.getItem("emailId");
  }

  ngOnInit() {
    this.service.cartItems$.subscribe((cartItems) => {
      console.log('Received updated cart items:', cartItems);
      this.cartItems = cartItems;
      this.calculateTotal();
      this.detectChanges(); // Manually trigger change detection
    });
  }

  private detectChanges() {
    if (!(this.cdr as any)['destroyed']) {
      this.cdr.detectChanges();
    }
  }

  calculateTotal() {
    this.total = this.cartItems.reduce((sum, item) => {
      const itemPrice = item.prodPrice || 0;
      const itemQuantity = item.quantity || 1;
      return sum + (itemPrice * itemQuantity);
    }, 0);
    console.log('Calculated total:', this.total);
  }


  checkOut() {
    localStorage.removeItem("cartProducts");
    this.cartItems = [];
    this.total = 0; // Reset total to 0
    alert("Thank You For Shopping With US");
  }

  getTotalPrice(): number {
    return this.total;
  }

  removeItem(item: any) {
    const index = this.cartItems.indexOf(item);
    if (index !== -1) {
      this.cartItems.splice(index, 1);
      this.calculateTotal(); // Recalculate total after removing an item
    }
  }

  emptyCart() {
    this.cartItems = [];
    this.total = 0;
  }

  orderNow() {
    this.router.navigate(['/order']);
  }
}

