import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipetComponent } from './recipet.component';

describe('RecipetComponent', () => {
  let component: RecipetComponent;
  let fixture: ComponentFixture<RecipetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecipetComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RecipetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
