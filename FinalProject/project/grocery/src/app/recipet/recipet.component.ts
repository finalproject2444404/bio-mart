import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipet',
  templateUrl: './recipet.component.html',
  styleUrls: ['./recipet.component.css']
})
export class RecipetComponent implements OnInit, OnDestroy {
  orderDetails: any;
  totalAmount: number = 0;
  totalAmountSubscription!: Subscription;

  constructor(private route: ActivatedRoute, private customerService: CustomerService) {}

  ngOnInit() {
    this.orderDetails = this.customerService.getOrderDetails();
    this.totalAmount = this.customerService.getTotalAmount();
    console.log('Order Details:', this.orderDetails);
    console.log('Total Amount:', this.totalAmount);

    this.totalAmountSubscription = this.customerService.totalAmount$.subscribe((totalAmount) => {
      this.totalAmount = totalAmount;
      console.log('Updated Total Amount:', this.totalAmount);
    });
  }

  ngOnDestroy() {
    if (this.totalAmountSubscription) {
      this.totalAmountSubscription.unsubscribe();
    }
  }
}



