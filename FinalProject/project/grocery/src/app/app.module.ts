import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { ItemsComponent } from './items/items.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { CareersComponent } from './careers/careers.component';
import { PaymentsComponent } from './payments/payments.component';
import { ShippingComponent } from './shipping/shipping.component';
import { CancreturnComponent } from './cancreturn/cancreturn.component';
import { CartComponent } from './cart/cart.component';
import { OrderComponent } from './order/order.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { RecipetComponent } from './recipet/recipet.component';
import { CustomerService } from './services/customer.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    MainComponent,
    LoginComponent,
    ItemsComponent,
    RecipeComponent,
    RegisterComponent,
    AdminComponent,
    AboutComponent,
    ContactComponent,
    CareersComponent,
    PaymentsComponent,
    ShippingComponent,
    CancreturnComponent,
    CartComponent,
    OrderComponent,
    PasswordresetComponent,
    RecipetComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
   
  ],
  bootstrap: [AppComponent],
  providers: [CustomerService],
})
export class AppModule {}

