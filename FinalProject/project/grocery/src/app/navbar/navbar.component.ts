import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {
  loginStatus: any;

  constructor(private service: CustomerService, private cdr: ChangeDetectorRef) {}

  ngOnInit() {
    this.service.getIsUserLoggedStatus().subscribe((data: any) => {
      console.log('Login Status:', data);
      this.loginStatus = data;
      this.cdr.detectChanges(); // Manually trigger change detection
    });
  }

  logout() {
    console.log('Logging out...');
    this.service.setLogoutStatus();
    this.cdr.detectChanges(); // Manually trigger change detection
  }
}


