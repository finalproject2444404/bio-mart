// order.component.ts

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orderForm!: FormGroup; // Add '!' to tell TypeScript that it will be initialized later

  constructor(private route: ActivatedRoute, private fb: FormBuilder,private router: Router, private customerService: CustomerService) {}

  ngOnInit() {
    this.initForm();
    this.populateForm();
  }

  private initForm() {
    this.orderForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      address: ['', Validators.required],
      phone: ['', Validators.required],
      paymenttype: ['', Validators.required],
    });
  }

  private populateForm() {
    const orderDetails = this.route.snapshot.paramMap.get('orderDetails');
    if (orderDetails) {
      // Convert the JSON string to an object
      const details = JSON.parse(orderDetails);

      this.orderForm.patchValue({
        name: details.name,
        email: details.email,
        address: details.address,
        phone: details.phone,
        paymenttype: details.paymenttype,
      });
    }
  }
  order() {
    const formValues = this.orderForm.value;
  
    // Log form values and items to the console
    console.log('Form Values:', formValues);
    console.log('Items:', formValues.items);
  
    // Calculate total price based on your business logic
    const totalPrice = this.calculateTotalPrice(formValues);
  
    // Log the total price to the console
    console.log('Total Price:', totalPrice);
  
    // Add the total price to the form values
    formValues.totalPrice = totalPrice;
  
    // Set order details including total price in the CustomerService
    this.customerService.setOrderDetails(formValues);
  
    // Navigate to the recipet page
    this.router.navigate(['/recipet']);
  }
  
  
  
  private calculateTotalPrice(orderDetails: any): number {
    // Implement your logic to calculate the total price
    // For example, you might sum the prices of items in the order
  
    // Assuming you have an array of items in the order
    const items: any[] = orderDetails.items; // Update this based on your actual structure
    let totalPrice = 0;
  
    if (items) {
      totalPrice = items.reduce((sum: number, item: any) => sum + (item.quantity * item.price), 0);
    }
  
    return totalPrice;
  }
  
  
  }




