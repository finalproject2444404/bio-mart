import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { ItemsComponent } from './items/items.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { CareersComponent } from './careers/careers.component';
import { PaymentsComponent } from './payments/payments.component';
import { ShippingComponent } from './shipping/shipping.component';
import { CancreturnComponent } from './cancreturn/cancreturn.component';
import { CartComponent } from './cart/cart.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { OrderComponent } from './order/order.component';
import { RecipetComponent } from './recipet/recipet.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: MainComponent },
  { path: 'items', component: ItemsComponent },
  { path: 'recipe', component: RecipeComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'careers', component: CareersComponent },
  { path: 'payments', component: PaymentsComponent },
  { path: 'shipping', component: ShippingComponent },
  { path: 'cancreturn', component: CancreturnComponent },
{path:'cart',component:CartComponent},
{path:'passwordreset',component:PasswordresetComponent},
{path:'order',component:OrderComponent},
{
  path: 'recipet',
  component: RecipetComponent,
  data: { orderDetails: null } // initialize with null or set a default value
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
