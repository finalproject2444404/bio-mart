import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent {

  recipes: any[] = [
    {
      recipeName: 'Pizza',
      recipeImage: 'assets/images/pizza.jpeg',
      ytLink: 'https://www.youtube.com/embed/sv3TXMSv6Lw?si=WXS7KAq6cQu-KJoL',
      showFullContent: false,
      embeddedLink: null as any // Initialize as null
    },
    {
      recipeName: 'ChickenCurry',
      recipeImage: 'assets/images/ChickenCurry.jpeg',
      ytLink: 'https://www.youtube.com/embed/0kxuFVG-Gh4?si=IyaBUcH8GCKmhNbT',
      showFullContent: false,
      embeddedLink: null as any // Initialize as null
    },
    {
      recipeName: 'Burger',
      recipeImage: 'assets/images/Burger.jpeg',
      ytLink: 'https://www.youtube.com/embed/m2dhUanzEp4?si=vOEogdDm2xhhqqY7',
      showFullContent: false,
      embeddedLink: null as any // Initialize as null
    },
    // Add more recipes as needed
  ];

  constructor(private sanitizer: DomSanitizer) {
    this.recipes.forEach(recipe => {
      recipe.embeddedLink = this.sanitizer.bypassSecurityTrustResourceUrl(recipe.ytLink) as SafeResourceUrl;
    });
  }
}


