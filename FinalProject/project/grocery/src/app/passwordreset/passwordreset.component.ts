import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../services/customer.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.css']
})
export class PasswordresetComponent implements OnInit {
  passwordForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: CustomerService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.passwordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      newPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]]
    });
  }

  ngOnInit() {}

  resetPassword() {
    const email = this.passwordForm.get('email')?.value;
    const newPassword = this.passwordForm.get('newPassword')?.value.trim();
    const confirmPassword = this.passwordForm.get('confirmPassword')?.value.trim();

    if (newPassword === confirmPassword) {
      this.service.resetPassword(email, newPassword).subscribe(
        (data: any) => {
          console.log('Password reset response:', data);

          if (data && data.status) {
            if (data.status === 'success') {
              this.toastr.success('Password change successful.', 'Success');
              this.router.navigate(['/login']);
              console.log('Password reset successful:', data.message);
            } else if (data.status === 'error') {
              this.toastr.error('Password change failed: ' + data.message, 'Error');
              console.error('Error resetting password:', data.message);
            } else {
              console.log('Unexpected response:', data);
            }
          }
        },
        (error: any) => {
          // Handle error
          this.toastr.error('Error resetting password. Please try again.', 'Error');
          console.error('Error resetting password:', error);
        }
      );
    } else {
      this.toastr.warning('Passwords do not match. Please try again.', 'Warning');
    }
  }
}

