import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancreturnComponent } from './cancreturn.component';

describe('CancreturnComponent', () => {
  let component: CancreturnComponent;
  let fixture: ComponentFixture<CancreturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancreturnComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CancreturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
