// items.component.ts

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css'],
})
export class ItemsComponent implements OnInit {
  items: any;
  showItemsDiv: boolean = true;
  showBackgroundImage: boolean = true;

  constructor(private service: CustomerService, private toastr: ToastrService) {}

  ngOnInit(): void {
    // Initialization logic here if needed
  }

  //addToCart(item: any) {
   // const existingItem = this.service.cartItems.find(
    //  (cartItem: any) => cartItem.prodName === item.prodName
   // );

   // if (existingItem) {
      // If the item already exists in the cart, increment the quantity
    //  existingItem.quantity += 1;
   // } else {
      // If the item is not in the cart, add it with a quantity of 1
    //  item.quantity = 1;
     // this.service.addToCart(item);
   // }
   // this.showSuccess();
 // }
 
 addToCart(item: any) {
  this.service.addToCart(item);
  this.showSuccess();
}
  showSuccess() {
    this.toastr.success('Added to Cart Successfully!!!', 'Added');
  }

  getVegetable() {
    this.service.getByCategoryVegetable().subscribe((data: any) => {
      this.items = data;
      this.showItemsDiv = false;
    });
  }

  getDairy() {
    this.service.getByCategoryDairy().subscribe((data: any) => {
      this.items = data;
      this.showItemsDiv = false;
      this.showBackgroundImage = false;
    });
  }

  getFruits() {
    this.service.getByCategoryFruits().subscribe((data: any) => {
      this.items = data;
      this.showItemsDiv = false;
      this.showBackgroundImage = false;
    });
  }

  getGrocery() {
    this.service.getByCategoryGrocery().subscribe((data: any) => {
      this.items = data;
      this.showItemsDiv = false;
      this.showBackgroundImage = false;
    });
  }

  increaseQuantity(item: any) {
    item.quantity = (item.quantity || 1) + 1;
  }

  decreaseQuantity(item: any) {
    if (item.quantity && item.quantity > 1) {
      item.quantity -= 1;
    }
  }
}
