// admin.component.ts

import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../services/customer.service';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  customers: any;
  custobj: any;
  items: any[] = [];
  updateForm: FormGroup;
  selectedItem: any;
  showUpdateForm: boolean = false;

  constructor(private service: CustomerService, private formBuilder: FormBuilder) {
    this.custobj = {
      "custName": '',
      "custPhone": '',
      "custEmail": '',
      "password": "",
      "confirmPassword": "",
    };
    this.updateForm = this.formBuilder.group({
      name: [''],
      email: [''],
      password: ['']
    });
  }

  ngOnInit(): void {
    this.service.getAllCust().subscribe(
      (data: any) => {
        console.log('Customer Data:', data);
        this.customers = data;
      },
      (error: HttpErrorResponse) => {
        console.error('Error fetching customer data:', error);

        if (error.status === 0) {
          console.error('Make sure your backend server is running and accessible.');
        } else {
          console.error('HTTP request failed. Status:', error.status, 'Message:', error.message);
        }
      }
    );
    console.log('ngOnInit called');
  }

  deleteCustomer(id: any) {
    this.service.deleteCustomer(id).subscribe(
      (data: any) => {
        console.log(data);
        // Refresh the customer list after deletion
        this.service.getAllCust().subscribe(
          (data: any) => {
            this.customers = data;
          },
          (error: HttpErrorResponse) => {
            console.error('Error fetching customer data after deletion:', error);
          }
        );
      },
      (error: HttpErrorResponse) => {
        console.error('Error deleting customer:', error);
        alert('Failed to delete customer. Please try again.');
      }
    );
  }

 // Inside AdminComponent class
// Inside AdminComponent class
updateItem(item: any) {
    this.selectedItem = item;
    this.updateForm.setValue({
      name: item.custName,
      email: item.custEmail,
      password: item.password
    });
    this.showUpdateForm = true;
  }
  
  submitUpdate() {
    const updatedItem = {
        id: this.selectedItem.id,
        ...this.updateForm.value
    };

    console.log('Updated item:', updatedItem); // Add this log

    this.service.updateCustomer(updatedItem).subscribe(
        () => {
            // Refresh the customer list after updating
            this.service.getAllCust().subscribe(
                (data: any) => {
                    this.customers = data;
                },
                (error: HttpErrorResponse) => {
                    console.error('Error fetching customer data after update:', error);
                }
            );

            this.showUpdateForm = false;
        },
        (error: any) => {
            console.error('Update failed:', error);
        }
    );
}


  private loadItems() {
    // Implement the logic to reload items after an update
  }
  
  

 
}

