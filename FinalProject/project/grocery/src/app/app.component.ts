import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  template: `
  <button (click)="showSuccess()">Show Success Toastr</button>
`,
})
export class AppComponent {
  title = 'grocery';
  constructor(private toastr: ToastrService) {}

  showSuccess() {
    this.toastr.success('Hello, this is a success toast!', 'Success');
  }
}
// app.component.ts





