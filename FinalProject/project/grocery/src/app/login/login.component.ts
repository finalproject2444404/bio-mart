// import { Component } from '@angular/core';


// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })
// export class LoginComponent {
//   passwordVisible = false;
//   actualPassword = '';
//   captchaText = this.generateCaptcha();

//   togglePasswordVisibility() {
//     this.passwordVisible = !this.passwordVisible;
//   }

//   generateCaptcha(): string {
//     const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//     let captcha = '';
//     for (let i = 0; i < 6; i++) {
//       captcha += characters.charAt(Math.floor(Math.random() * characters.length));
//     }
//     return captcha;
//   }

//   loginSubmit(formData: any) {
//     // Implement your login logic here
//     // formData will contain email, password, and captcha
//   }
// }






import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  emailId: any;
  password: any;
  captcha: string = '';
  captchaInput: string = '';
  captchaText: string = '';

  constructor(private router: Router, private service: CustomerService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.emailId = '';
    this.password = '';
    this.generateCaptcha();
  }

  async loginSubmit() {
    try {
      const data = await this.service.custLogin({ emailId: this.emailId, password: this.password }).toPromise();
      console.log('Full Service Response:', data);

      if (data) {
        if (this.emailId === 'admin@gmail.com' && this.password === 'adminpassword') {
          // Navigate to the admin page
          this.router.navigate(['/admin']);
          return;
        }
        this.showSuccess();
       // this.toastr.success('Login Successful', 'Success');
        this.router.navigate(['/items']);
        this.service.setLoginStatus();
      } else {
        //this.toastr.error('Login Failed. Invalid Credentials', 'Error');
        this.showUnSuccess();
      }
    } catch (error) {
      console.error('Service Error:', error);
     // this.toastr.error('Login Failed. Please try again.', 'Error');
    }
  }
  showSuccess() {
    this.toastr.success('Successfully logged in!!!', 'Success');
  }
  showUnSuccess() {
    this.toastr.error('Login Unsuccessful', 'Error');
  }
  generateCaptcha(): void {
    const possibleChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const length = 6;
    this.captcha = Array.from({ length }, () =>
      possibleChars[Math.floor(Math.random() * possibleChars.length)]
    ).join('');
  }
}

